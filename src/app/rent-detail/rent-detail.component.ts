import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { RentalService } from '../rental.service';
import {Location} from '@angular/common';
import { Rental } from '../rentals';

@Component({
  selector: 'app-rent-detail',
  templateUrl: './rent-detail.component.html',
  styleUrls: ['./rent-detail.component.css']
})
export class RentDetailComponent implements OnInit {

  rental: Rental;

  constructor(
    private route: ActivatedRoute,
    private rentalService: RentalService,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.getRental();
  }

  getRental(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.rentalService.getRental(id).subscribe(rental => this.rental = rental);
  }

  goBack(): void {
    this.location.back();
  }

  save(): void {

    this.rentalService.updateRental(this.rental)
      .subscribe(() => this.goBack());
  }
}
