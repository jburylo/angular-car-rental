import {Component, Input, OnInit } from '@angular/core';
import {Rental} from '../rentals';
import { RentalService } from '../rental.service';
import { AdminService } from '../admin.service';
import { Router} from '@angular/router';
import {Customer} from '../customer';
import {CustomerService} from '../customer.service';
import { CarReservationService } from '../car-reservation.service';
import {Car} from '../car';
import { CarService } from '../car.service';

@Component({
  selector: 'app-rentals',
  templateUrl: './rentals.component.html',
  styleUrls: ['./rentals.component.css']
})
export class RentalsComponent implements OnInit {

  rentals: Rental[];
  customer: Customer;
  customers: Customer[];
  car: Car;
  cars: Car[];

  dateTo: Date;
  dateFrom: Date;

  rental: Rental;

  key = 'name';
  reverse = false;
  sort(key) {
    this.key = key;
    this.reverse = !this.reverse;
  }

  getRentals(): void {
    this.rentalService.getRentals().subscribe(rentals => this.rentals = rentals);
  }

  getCustomers(): void {
    this.customerService.getCustomers().subscribe(customers => this.customers = customers);
  }

  getCars(): void {
    this.carService.getCars().subscribe(cars => this.cars = cars);
  }

  constructor(
    private rentalService: RentalService,
    private adminService: AdminService,
    private router: Router,
    private customerService: CustomerService,
    private carReservationService: CarReservationService,
    private carService: CarService,
  ) { }

  logout(): void {
    this.adminService.setAdminLoggedOut();
    this.router.navigate(['/']);
  }

  delete(rental: Rental): void {
    this.rentals = this.rentals.filter(r => r !== rental);
    this.rentalService.deleteRental(rental).subscribe();

    const id = Number(rental.customerId) - 1;
    const carId = Number(rental.carId);
    for (let i = 0; i < this.customers[id].currentRentals.length; i++) {
      if (this.customers[id].currentRentals[i] === carId) {
        this.customers[id].currentRentals.splice(i, 1);
      }
    }
    this.customerService.updateCustomer(this.customers[id]).subscribe();
  }

  ngOnInit() {
    this.getRentals();
    this.getCars();
    this.getCustomers();
  }

  add(customerId: number, carId: number, dateFrom: Date,
               dateTo: Date): void {

    if (this.carReservationService.validateIfDateFromIsBeforeDateTo(this.dateFrom, this.dateTo) &&
      this.carReservationService.validateIfCarIsNotReservedOnThisDate(this.dateFrom, this.dateTo, carId, this.rentals) &&
      this.carReservationService.
            validateIfServiceDateIsNotBetweenDateFromAndDateToSecondVersion(this.dateFrom, this.dateTo, carId, this.cars)
      // && this.carReservationService.validateIfUserExist(customerId, this.customers)
    // && this.carReservationService.validateIfCarExist (carId, this.cars)
    ) {

    //   // alert('walidacja daty dziala');

      if (!customerId || !carId || !dateFrom || !dateTo) {
        return;
      }
      this.rentalService.addRental({ customerId, carId, dateFrom, dateTo} as Rental)
        .subscribe(rental => {
          this.rentals.push(rental);
        }, );

      this.customers[customerId - 1].currentRentals.push(Number(carId));
      this.customerService.updateCustomer(this.customers[customerId - 1]).subscribe();
    }
  }

  onValueChangeFrom(value: Date): void {
    this.dateFrom = value;
  }

  onValueChangeTo(value: Date): void {
    this.dateTo = value;
  }
}
