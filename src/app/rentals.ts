export class Rental {
  id: number;
  customerId: number;
  carId: number;
  dateFrom: Date;
  dateTo: Date;
  status: string;
  wifi: string;
  gps: string;
  additionalInsurance: string;
}
