import { InMemoryDbService } from 'angular-in-memory-web-api';
import {Car} from './car';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const cars: Car[] = [
      {id: 1,
        brand: 'Toyota',
        model: 'Auris',
        description: 'Błękitna strzała',
        colour: 'sky blue',
        dateOfProduction: new Date('06/11/2012'),
        dateOfRetirement: new Date('12/15/2015'),
        mileage: 123435,
        servicePeriod: new Date('06/23/2019'),
        status: 'available'
      },

      {id: 2,
        brand: 'Skoda',
        model: 'Fabia',
        description: 'Srebrny morderca',
        colour: 'silver',
        dateOfProduction: new Date('12/15/2006'),
        dateOfRetirement: new Date('12/15/2020'),
        mileage: 123435,
        servicePeriod: new Date('12/15/2019'),
        status: 'available'
      },

      {id: 3,
        brand: 'Ferrari',
        model: '488 GTB',
        description: 'Czerwony koń',
        colour: 'red',
        dateOfProduction: new Date('11/11/2011'),
        dateOfRetirement: new Date('11/11/2023'),
        mileage: 123435,
        servicePeriod: new Date('12/15/2019'),
        status: 'reserved'
      },

      {id: 4,
        brand: 'Fiat',
        model: '126p',
        description: 'HanksMobil',
        colour: 'red',
        dateOfProduction: new Date('12/15/1982'),
        dateOfRetirement: new Date('12/15/2023'),
        mileage: 123435,
        servicePeriod: new Date('12/15/2019'),
        status: 'reserved'
      },

      {id: 5,
        brand: 'BMW',
        model: 'X6',
        description: 'Czarny Niemiec',
        colour: 'black',
        dateOfProduction: new Date('12/15/2011'),
        dateOfRetirement: new Date('12/15/2023'),
        mileage: 123435,
        servicePeriod: new Date('12/15/2019'),
        status: 'available'
      },

      {id: 6,
        brand: 'Syrena',
        model: '105',
        description: 'Polski klasyk',
        colour: 'white',
        dateOfProduction: new Date('12/15/2011'),
        dateOfRetirement: new Date('12/15/2023'),
        mileage: 123435,
        servicePeriod: new Date('12/15/2019'),
        status: 'serviced'
      },

      {id: 7,
        brand: 'BMW',
        model: 'M5 F10',
        description: 'Czarny Niemiec',
        colour: 'black',
        dateOfProduction: new Date('12/15/2011'),
        dateOfRetirement: new Date('12/15/2023'),
        mileage: 123435,
        servicePeriod: new Date('12/15/2019'),
        status: 'serviced'
      },
    ];

    const customers = [
      {id: 1,
        name: 'Jakub',
        surname: 'Buryło',
        email: 'jakub.burylo@rentalcars.com',
        creditCardNumber: 12418725032,
        login: 'user1',
        password: 'user1',
        currentRentals: [7]
      },

      {id: 2,
        name: 'Marian',
        surname: 'Patyk',
        email: 'marian.patyk@rentalcars.com',
        creditCardNumber: 12124651116,
        login: 'user2',
        password: 'user2',
        currentRentals: []
      },

      {id: 3,
        name: 'Janusz',
        surname: 'Nosacz',
        email: 'janusz.nosacz@rentalcars.com',
        creditCardNumber: 12161618932,
        login: 'user3',
        password: 'user3',
        currentRentals: [2]
      },

      {id: 4,
        name: 'Ferdek',
        surname: 'Kiepski',
        email: 'ferdek.kiepski@rentalcars.com',
        creditCardNumber: 36364425032,
        login: 'user4',
        password: 'user4',
        currentRentals: [5]
      }
    ];

    const rentals = [
      {id: 1,
        customerId: 1,
        carId: 7,
        dateFrom: new Date('05/10/2018'),
        dateTo: new Date('05/25/2018'),
        status: 'accepted',
        wifi: 'YES',
        gps: 'YES',
        additionalInsurance: 'YES'
      },

      {id: 2,
        customerId: 3,
        carId: 2,
        dateFrom: new Date('07/15/2018'),
        dateTo: new Date('08/25/2018'),
        status: 'accepted',
        wifi: 'NO',
        gps: 'YES',
        additionalInsurance: 'NO'
      },

      {id: 3,
        customerId: 4,
        carId: 5,
        dateFrom: new Date('01/19/2018'),
        dateTo: new Date('02/15/2018'),
        status: 'declined',
        wifi: 'NO',
        gps: 'NO',
        additionalInsurance: 'NO'
      }
    ];

    return {cars, customers, rentals};
    // return {cars};
  }
}
