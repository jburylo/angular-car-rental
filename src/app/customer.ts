export class Customer {
  id: number;
  name: string;
  surname: string;
  email: string;
  creditCardNumber: number;
  login: string;
  password: string;
  currentRentals: number[];
}
