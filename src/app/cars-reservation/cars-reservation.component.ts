import {Component, OnInit } from '@angular/core';
import {Car} from '../car';
import { CarService } from '../car.service';
import { CustomerService } from '../customer.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cars-reservation',
  templateUrl: './cars-reservation.component.html',
  styleUrls: ['./cars-reservation.component.css']
})

export class CarsReservationComponent implements OnInit {

  cars: Car[];

  key = 'name';
  reverse = false;
  sort(key) {
    this.key = key;
    this.reverse = !this.reverse;
  }

  getCars(): void {
    this.carService.getCars().subscribe(cars => this.cars = cars);
  }

  logout(): void {
    this.customerService.setCustomerLoggedOut();
    this.customerService.setCustomerId(-1);
    this.router.navigate(['/']);
  }

  constructor(
    private carService: CarService,
    private customerService: CustomerService,
    private router: Router,
    ) { }

  ngOnInit() {
    this.getCars();
  }
}
