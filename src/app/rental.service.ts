import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Rental } from './rentals';
import {HttpClient, HttpHeaders} from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class RentalService {

  constructor(private http: HttpClient) { }

  rentalsUrl = 'api/rentals';  // URL to web api

  getRentals (): Observable<Rental[]> {
    return this.http.get<Rental[]>(this.rentalsUrl);
  }

  getRental(id: number): Observable<Rental> {
    const url = `${this.rentalsUrl}/${id}`;
    return this.http.get<Rental>(url);
  }

  updateRental (rental: Rental): Observable<any> {
    return this.http.put(this.rentalsUrl, rental, httpOptions);
  }

  addRental (rental: Rental): Observable<Rental> {
    return this.http.post<Rental>(this.rentalsUrl, rental, httpOptions);
  }

  deleteRental (rental: Rental | number): Observable<Rental> {
    const id = typeof rental === 'number' ? rental : rental.id;
    const url = `${this.rentalsUrl}/${id}`;

    return this.http.delete<Rental>(url, httpOptions);
  }

}
