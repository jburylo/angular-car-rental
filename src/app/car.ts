
export class Car {
  id: number;
  brand: string;
  model: string;
  description: string;
  colour: string;
  dateOfProduction: Date;
  dateOfRetirement: Date;
  mileage: number;
  servicePeriod: Date;
  status: string;
  // status: Status.available;
}
