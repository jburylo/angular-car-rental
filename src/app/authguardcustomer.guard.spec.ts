import { TestBed, async, inject } from '@angular/core/testing';

import { AuthguardcustomerGuard } from './authguardcustomer.guard';
import {CustomerService} from './customer.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('AuthguardcustomerGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers: [
        AuthguardcustomerGuard,
        CustomerService
      ]
    });
  });

  it('should ...', inject([AuthguardcustomerGuard], (guard: AuthguardcustomerGuard) => {
    expect(guard).toBeTruthy();
  }));
});
