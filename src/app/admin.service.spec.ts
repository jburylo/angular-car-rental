import { TestBed, inject } from '@angular/core/testing';

import { AdminService } from './admin.service';

describe('AdminService', () => {

  let adminService: AdminService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminService]
    });
    adminService = TestBed.get(AdminService);
  });

  it('should be created', inject([AdminService], (service: AdminService) => {
    expect(service).toBeTruthy();
  }));

  it('should AdminLoggedIn be false', inject([AdminService], (service: AdminService) => {
    // when
    expect(adminService.getAdminLoggedIn()).toBeFalsy();
  }));

  it('should set AdminLoggedIn to be true', inject([AdminService], (service: AdminService) => {
    // when
    adminService.setAdminLoggedIn();
    expect(adminService.getAdminLoggedIn()).toBeTruthy();
  }));

  it('should set AdminLoggedIn to be false', inject([AdminService], (service: AdminService) => {
    // when
    adminService.setAdminLoggedIn();
    adminService.setAdminLoggedOut();
    expect(adminService.getAdminLoggedIn()).toBeFalsy();
  }));
});
