import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Customer } from './customer';
import {HttpClient, HttpHeaders} from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class CustomerService {

  isCustomerLoggedIn;
  private currentCustomerId;

  constructor(private http: HttpClient) {
    this.isCustomerLoggedIn = false;
  }

  customersUrl = 'api/customers';  // URL to web api

  setCustomerLoggedIn() {
    this.isCustomerLoggedIn = true;
  }

  setCustomerLoggedOut() {
    this.isCustomerLoggedIn = false;
  }

  getCustomerLoggedIn() {
    return this.isCustomerLoggedIn;
  }

  setCustomerId(i) {
    this.currentCustomerId = i;
  }

  getCustomerId() {
    return this.currentCustomerId;
  }

  getCustomerIdInString() {
    return this.currentCustomerId.toString();
  }

  getCustomers (): Observable<Customer[]> {
    return this.http.get<Customer[]>(this.customersUrl);
  }

  getCustomer(id: number): Observable<Customer> {
    const url = `${this.customersUrl}/${id}`;
    return this.http.get<Customer>(url);
  }

  updateCustomer (customer: Customer): Observable<any> {
    return this.http.put(this.customersUrl, customer, httpOptions);
  }

  addCustomer (customer: Customer): Observable<Customer> {
    return this.http.post<Customer>(this.customersUrl, customer, httpOptions);
  }

  deleteCustomer (customer: Customer | number): Observable<Customer> {
    const id = typeof customer === 'number' ? customer : customer.id;
    const url = `${this.customersUrl}/${id}`;

    return this.http.delete<Customer>(url, httpOptions);
  }
}
