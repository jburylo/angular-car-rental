import { TestBed, inject } from '@angular/core/testing';

import { CustomerService } from './customer.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Customer} from './customer';

describe('CustomerService', () => {

  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let customerService: CustomerService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers: [CustomerService]
    });
    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    customerService = TestBed.get(CustomerService);
  });

  it('should be created', inject([CustomerService], (service: CustomerService) => {
    expect(service).toBeTruthy();
  }));

  /// CarService method tests begin ///
  describe('#getCustomers', () => {

    let expectedCustomers: Customer[];
    let expectedCustomer: Customer;

    beforeEach(() => {
      expectedCustomers = [
        {id: 1,
          name: 'Jakub',
          surname: 'Buryło',
          email: 'jakub.burylo@rentalcars.com',
          creditCardNumber: 12418725032,
          login: 'user1',
          password: 'user1',
          currentRentals: [7]
        },

        {id: 2,
          name: 'Marian',
          surname: 'Patyk',
          email: 'marian.patyk@rentalcars.com',
          creditCardNumber: 12124651116,
          login: 'user2',
          password: 'user2',
          currentRentals: []
        },

        {id: 3,
          name: 'Janusz',
          surname: 'Nosacz',
          email: 'janusz.nosacz@rentalcars.com',
          creditCardNumber: 12161618932,
          login: 'user3',
          password: 'user3',
          currentRentals: [2]
        },

        {id: 4,
          name: 'Ferdek',
          surname: 'Kiepski',
          email: 'ferdek.kiepski@rentalcars.com',
          creditCardNumber: 36364425032,
          login: 'user4',
          password: 'user4',
          currentRentals: [5]
        }
      ] as Customer[];
      expectedCustomer = {
          id: 3,
          name: 'Janusz',
          surname: 'Nosacz',
          email: 'janusz.nosacz@rentalcars.com',
          creditCardNumber: 12161618932,
          login: 'user3',
          password: 'user3',
          currentRentals: [2]
        };
    });

    it('should return expected customers (called once)', () => {
      customerService.getCustomers().subscribe(
        c => expect(c).toEqual(expectedCustomers, 'should return expected customers'),
        fail
      );

      // CustomerService should have made one request to GET customers from expected URL
      const req = httpTestingController.expectOne(customerService.customersUrl);
      expect(req.request.method).toEqual('GET');

    });

    it('should return expected customers (called multiple times)', () => {
      customerService.getCustomers().subscribe();
      customerService.getCustomers().subscribe();
      customerService.getCustomers().subscribe(
        heroes => expect(heroes).toEqual(expectedCustomers, 'should return expected customers'),
        fail
      );

      const requests = httpTestingController.match(customerService.customersUrl);
      expect(requests.length).toEqual(3, 'calls to getHeroes()');

    });

  });

  describe('#getCustomer', () => {

    it('should return expected customer', () => {
      const newCustomer = {
        id: 1,
        name: 'Janusz',
        surname: 'Nosacz',
        email: 'janusz.nosacz@rentalcars.com',
        creditCardNumber: 12161618932,
        login: 'user3',
        password: 'user3',
        currentRentals: [2]
      };
      customerService.addCustomer(newCustomer);
      customerService.getCustomer(1).subscribe(
        c => expect(c).toEqual(newCustomer, 'should return expected customer'),
        fail
      );

      // CustomerService should have made one request to GET customers from expected URL
      const req = httpTestingController.expectOne(customerService.customersUrl + '/1');
      expect(req.request.method).toEqual('GET');

    });
  });

  describe('#updateCustomer', () => {

    it('should update a customer and return it', () => {

      const updateCustomer: Customer = {
        id: 1,
        name: 'Janusz',
        surname: 'Nosacz',
        email: 'janusz.nosacz@rentalcars.com',
        creditCardNumber: 12161618932,
        login: 'user3',
        password: 'user3',
        currentRentals: [2]
      };

      customerService.updateCustomer(updateCustomer).subscribe(
        data => expect(data).toEqual(updateCustomer, 'should return the customer'),
        fail
      );

      // CustomerService should have made one request to PUT cusotmer
      const req = httpTestingController.expectOne(customerService.customersUrl);
      expect(req.request.method).toEqual('PUT');
      expect(req.request.body).toEqual(updateCustomer);

    });

  });

  describe('#addCustomer', () => {

    it('should add a customer and return it', () => {

      const addCustomer: Customer = {
        id: 1,
        name: 'Janusz',
        surname: 'Nosacz',
        email: 'janusz.nosacz@rentalcars.com',
        creditCardNumber: 12161618932,
        login: 'user3',
        password: 'user3',
        currentRentals: [2]
      };

      customerService.addCustomer(addCustomer).subscribe(
        data => expect(data).toEqual(addCustomer, 'should return the customer'),
        fail
      );

      // CustomerService should have made one request to POST customer
      const req = httpTestingController.expectOne(customerService.customersUrl);
      expect(req.request.method).toEqual('POST');
      expect(req.request.body).toEqual(addCustomer);

    });
  });

  describe('#deleteCar', () => {

    it('should delete a car and return it', () => {

      const addCustomer: Customer = {
        id: 1,
        name: 'Janusz',
        surname: 'Nosacz',
        email: 'janusz.nosacz@rentalcars.com',
        creditCardNumber: 12161618932,
        login: 'user3',
        password: 'user3',
        currentRentals: [2]
      };

      customerService.addCustomer(addCustomer);

      customerService.deleteCustomer(addCustomer).subscribe(
        data => expect(data).toEqual(addCustomer, 'should return the car'),
        fail
      );

      // CarService should have made one request to POST car
      const req = httpTestingController.expectOne(customerService.customersUrl + '/1');
      expect(req.request.method).toEqual('DELETE');
    });
  });

});
