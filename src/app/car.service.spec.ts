import { TestBed, inject } from '@angular/core/testing';
import { HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule} from '@angular/common/http/testing';


import { CarService } from './car.service';
import {Car} from './car';

describe('CarService', () => {

  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let carService: CarService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      // Import the HttpClient mocking services
      imports: [ HttpClientTestingModule ],
      // Provide the service-under-test
      providers: [ CarService ]
    });
    // Inject the http, test controller, and service-under-test
    // as they will be referenced by each test.
    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    carService = TestBed.get(CarService);
  });

  it('should be created', inject([CarService], (service: CarService) => {
    expect(service).toBeTruthy();
  }));

  /// CarService method tests begin ///
  describe('#getCars', () => {

    let expectedCars: Car[];

    beforeEach(() => {
      carService = TestBed.get(CarService);
      expectedCars = [
        { id: 1,
          brand: 'Toyota',
          model: 'Auris',
          description: 'Błękitna strzała',
          colour: 'sky blue',
          dateOfProduction: new Date('12/15/2011'),
          dateOfRetirement: new Date('12/15/2023'),
          mileage: 123435,
          servicePeriod: new Date('12/15/2019'),
          status: 'available'
        },
        { id: 2,
          brand: 'Skoda',
          model: 'Fabia',
          description: 'Srebrny morderca',
          colour: 'silver',
          dateOfProduction: new Date('12/15/2006'),
          dateOfRetirement: new Date('12/15/2020'),
          mileage: 123435,
          servicePeriod: new Date('12/15/2019'),
          status: 'available'
        },
      ] as Car[];
    });

    it('should return expected cars (called once)', () => {
      carService.getCars().subscribe(
        cars => expect(cars).toEqual(expectedCars, 'should return expected cars'),
        fail
      );

      // CarService should have made one request to GET cars from expected URL
      const req = httpTestingController.expectOne(carService.carsUrl);
      expect(req.request.method).toEqual('GET');

    });

    it('should return expected cars (called multiple times)', () => {
      carService.getCars().subscribe();
      carService.getCars().subscribe();
      carService.getCars().subscribe(
        heroes => expect(heroes).toEqual(expectedCars, 'should return expected cars'),
        fail
      );

      const requests = httpTestingController.match(carService.carsUrl);
      expect(requests.length).toEqual(3, 'calls to getCars()');

    });
  });

  describe('#getCar', () => {

    it('should get car', () => {

      const newCar: Car = {
        id: 1,
        brand: 'Toyota',
        model: 'Auris',
        description: 'Błękitna strzała',
        colour: 'sky blue',
        dateOfProduction: new Date('12/15/2011'),
        dateOfRetirement: new Date('12/15/2023'),
        mileage: 123435,
        servicePeriod: new Date('12/15/2019'),
        status: 'available'
      };

      carService.addCar(newCar);

      carService.getCar(1).subscribe(
        data => expect(data).toEqual(newCar, 'should return the car'),
        fail
      );

      const req = httpTestingController.expectOne(carService.carsUrl + '/1');
      expect(req.request.method).toEqual('GET');
    });
  });

  describe('#updateCar', () => {

    it('should update a car and return it', () => {

      const updateCar: Car = {
        id: 1,
        brand: 'Toyota',
        model: 'Auris',
        description: 'Błękitna strzała',
        colour: 'sky blue',
        dateOfProduction: new Date('12/15/2011'),
        dateOfRetirement: new Date('12/15/2023'),
        mileage: 123435,
        servicePeriod: new Date('12/15/2019'),
        status: 'available'
      };

      carService.updateCar(updateCar).subscribe(
        data => expect(data).toEqual(updateCar, 'should return the car'),
        fail
      );

      // CarService should have made one request to PUT hero
      const req = httpTestingController.expectOne(carService.carsUrl);
      expect(req.request.method).toEqual('PUT');
      expect(req.request.body).toEqual(updateCar);

    });

  });

  describe('#addCar', () => {

    it('should add a car and return it', () => {

      const addCar: Car = {
        id: 1,
        brand: 'Toyota',
        model: 'Auris',
        description: 'Błękitna strzała',
        colour: 'sky blue',
        dateOfProduction: new Date('12/15/2011'),
        dateOfRetirement: new Date('12/15/2023'),
        mileage: 123435,
        servicePeriod: new Date('12/15/2019'),
        status: 'available'
      };

      carService.addCar(addCar).subscribe(
        data => expect(data).toEqual(addCar, 'should return the car'),
        fail
      );

      // CarService should have made one request to POST car
      const req = httpTestingController.expectOne(carService.carsUrl);
      expect(req.request.method).toEqual('POST');
      expect(req.request.body).toEqual(addCar);

    });
  });

  describe('#deleteCar', () => {

    it('should delete a car and return it', () => {

      const newCar: Car = {
        id: 1,
        brand: 'Toyota',
        model: 'Auris',
        description: 'Błękitna strzała',
        colour: 'sky blue',
        dateOfProduction: new Date('12/15/2011'),
        dateOfRetirement: new Date('12/15/2023'),
        mileage: 123435,
        servicePeriod: new Date('12/15/2019'),
        status: 'available'
      };

      carService.addCar(newCar);

      carService.deleteCar(newCar).subscribe(
        data => expect(data).toEqual(newCar, 'should return the car'),
        fail
      );

      // CarService should have made one request to POST car
      const req = httpTestingController.expectOne(carService.carsUrl + '/1');
      expect(req.request.method).toEqual('DELETE');
    });
  });
});
