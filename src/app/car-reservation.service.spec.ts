import { TestBed, inject } from '@angular/core/testing';

import { CarReservationService } from './car-reservation.service';
import {Car} from './car';
import { Rental } from './rentals';

describe('CarReservationService', () => {

  let carReservationService: CarReservationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CarReservationService]
    });
    carReservationService = TestBed.get(CarReservationService);
  });

  it('should be created', inject([CarReservationService], (service: CarReservationService) => {
    expect(service).toBeTruthy();
  }));

  describe('#validateIfDateFromIsBeforeDateTo', () => {

    let dateFrom: Date;
    let dateTo: Date;
    let dateFrom2: Date;
    let dateTo2: Date;

    beforeEach(() => {
      spyOn(window, 'alert');
      dateFrom = new Date('06/11/2018');
      dateTo = new Date('07/12/2018');
      dateFrom2 = dateTo;
      dateTo2 = dateFrom;
    });

    it('should return true when date from is before date to', inject([CarReservationService], (service: CarReservationService) => {
      // given
      // when
      // then
      expect(carReservationService.validateIfDateFromIsBeforeDateTo(dateFrom, dateTo)).toBeTruthy();
    }));

    it('should return false when date from is after date to', inject([CarReservationService], (service: CarReservationService) => {
      // given
      // when
      // then
      expect(carReservationService.validateIfDateFromIsBeforeDateTo(dateFrom2, dateTo2)).toBeFalsy();
      expect(window.alert).toHaveBeenCalledWith('Starting date cannot be before end date!');
    }));
  });

  describe('#validateIfServiceDateIsNotBetweenDateFromAndDateTo', () => {

    let dateFrom: Date;
    let dateTo: Date;

    beforeEach(() => {
      spyOn(window, 'alert');
      dateFrom = new Date('06/11/2018');
      dateTo = new Date('07/12/2018');
    });

    it('should return true when service date is not between dateFrom and dateTo',
      inject([CarReservationService], (service: CarReservationService) => {
      // given
      const serviceDate1 = new Date('13/12/2018');
      const serviceDate2 = new Date('05/12/2018');
      // when
      // then
      expect(carReservationService.validateIfServiceDateIsNotBetweenDateFromAndDateTo(dateFrom, dateTo, serviceDate1)).toBeTruthy();
      expect(carReservationService.validateIfServiceDateIsNotBetweenDateFromAndDateTo(dateFrom, dateTo, serviceDate2)).toBeTruthy();
    }));

    it('should return false when service date is between dateFrom and dateTo',
      inject([CarReservationService], (service: CarReservationService) => {
      // given
      const serviceDate = new Date('06/20/2018');
      // when
      // then
      expect(carReservationService.validateIfServiceDateIsNotBetweenDateFromAndDateTo(dateFrom, dateTo, serviceDate)).toBeFalsy();
      expect(window.alert).toHaveBeenCalledWith('You cannot rent this car! Between dates you have picked, car is servicing!');
    }));
  });

  describe('#validateIfServiceDateIsNotBetweenDateFromAndDateToSecondVersion', () => {

    let cars: Car[];
    let carId: Number;
    let dateFrom: Date;
    let dateTo: Date;
    let dateFrom2: Date;
    let dateTo2: Date;

    beforeEach(() => {
      spyOn(window, 'alert');
      carId = Number(3);
      dateFrom = new Date('06/11/2018');
      dateTo = new Date('07/12/2018');
      dateFrom2 = new Date('01/12/2019');
      dateTo2 = new Date('01/01/2020');

      cars = [
        {id: 1,
          brand: 'Maluszek',
          model: 'Auris',
          description: 'Błękitna strzała',
          colour: 'sky blue',
          dateOfProduction: new Date('06/11/2012'),
          dateOfRetirement: new Date('12/15/2015'),
          mileage: 123435,
          servicePeriod: new Date('06/23/2019'),
          status: 'available'
        },

        {id: 2,
          brand: 'Skoda',
          model: 'Fabia',
          description: 'Srebrny morderca',
          colour: 'silver',
          dateOfProduction: new Date('12/15/2006'),
          dateOfRetirement: new Date('12/15/2020'),
          mileage: 123435,
          servicePeriod: new Date('12/15/2019'),
          status: 'available'
        },

        {id: 3,
          brand: 'Ferrari',
          model: '488 GTB',
          description: 'Czerwony koń',
          colour: 'red',
          dateOfProduction: new Date('11/11/2011'),
          dateOfRetirement: new Date('11/11/2023'),
          mileage: 123435,
          servicePeriod: new Date('06/20/2019'),
          status: 'reserved'
        },

        {id: 4,
          brand: 'Fiat',
          model: '126p',
          description: 'HanksMobil',
          colour: 'red',
          dateOfProduction: new Date('12/15/1982'),
          dateOfRetirement: new Date('12/15/2023'),
          mileage: 123435,
          servicePeriod: new Date('12/15/2019'),
          status: 'reserved'
        },

        {id: 5,
          brand: 'BMW',
          model: 'X6',
          description: 'Czarny Niemiec',
          colour: 'black',
          dateOfProduction: new Date('12/15/2011'),
          dateOfRetirement: new Date('12/15/2023'),
          mileage: 123435,
          servicePeriod: new Date('12/15/2019'),
          status: 'available'
        },
      ] as Car[];
    });

    it('should return true when service date is not between dateFrom and dateTo',
      inject([CarReservationService], (service: CarReservationService) => {
        // given
        // when
        // then
        expect(carReservationService.validateIfServiceDateIsNotBetweenDateFromAndDateToSecondVersion(dateFrom, dateTo, carId, cars))
          .toBeTruthy();
      }));

    it('should return false when service date is between dateFrom and dateTo',
      inject([CarReservationService], (service: CarReservationService) => {
        // given
        // when
        // then
        expect(carReservationService.validateIfServiceDateIsNotBetweenDateFromAndDateToSecondVersion(dateFrom2, dateTo2, carId, cars))
          .toBeFalsy();
        expect(window.alert).toHaveBeenCalledWith('You cannot rent this car! Between dates you have picked, car is servicing!');
      }));
  });

  describe('#validateIfCarIsNotReservedOnThisDate', () => {

    let rentals: Rental[];
    let carId: Number;
    let dateFrom: Date;
    let dateTo: Date;
    let dateFrom2: Date;
    let dateTo2: Date;

    beforeEach(() => {
      spyOn(window, 'alert');
      carId = Number(5);
      dateFrom = new Date('01/17/2018');
      dateTo = new Date('02/12/2018');
      dateFrom2 = new Date('03/17/2018');
      dateTo2 = new Date('05/12/2018');

      rentals = [
        {id: 1,
          customerId: 1,
          carId: 7,
          dateFrom: new Date('05/10/2018'),
          dateTo: new Date('05/25/2018'),
          status: 'accepted',
          wifi: 'YES',
          gps: 'YES',
          additionalInsurance: 'YES'
        },

        {id: 2,
          customerId: 3,
          carId: 2,
          dateFrom: new Date('07/15/2018'),
          dateTo: new Date('08/25/2018'),
          status: 'accepted',
          wifi: 'NO',
          gps: 'YES',
          additionalInsurance: 'NO'
        },

        {id: 3,
          customerId: 4,
          carId: 5,
          dateFrom: new Date('01/19/2018'),
          dateTo: new Date('02/15/2018'),
          status: 'declined',
          wifi: 'NO',
          gps: 'NO',
          additionalInsurance: 'NO'
        }
      ] as Rental[];
    });

    it('should return false when car is reserved on demand date',
      inject([CarReservationService], (service: CarReservationService) => {
        // given
        // when
        // then
        expect(carReservationService.validateIfCarIsNotReservedOnThisDate(dateFrom, dateTo, carId, rentals))
          .toBeFalsy();
        expect(window.alert).toHaveBeenCalledWith('This car is already reserved in this date! Please choose another date!');
      }));

    it('should return true when car is not reserved on demand date',
      inject([CarReservationService], (service: CarReservationService) => {
        // given
        // when
        // then
        expect(carReservationService.validateIfCarIsNotReservedOnThisDate(dateFrom2, dateTo2, carId, rentals))
          .toBeTruthy();
      }));
  });

});
