import {HttpClient, HttpResponse } from '@angular/common/http';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import { HttpTestingController } from '@angular/common/http/testing';
import { RentalService} from './rental.service';
import { Rental} from './rentals';
import {inject, TestBed} from '@angular/core/testing';

describe('RentalService', () => {

  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let rentalService: RentalService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      // Import the HttpClient mocking services
      imports: [ HttpClientTestingModule ],
      // Provide the service-under-test
      providers: [ RentalService ]
    });
    // Inject the http, test controller, and service-under-test
    // as they will be referenced by each test.
    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    rentalService = TestBed.get(RentalService);
  });

  it('should be created', inject([RentalService], (service: RentalService) => {
    expect(service).toBeTruthy();
  })
  );

  /// RentalService method tests begin ///
  describe('#getRentals', () => {

    let expectedRentals: Rental[];

    beforeEach(() => {
      rentalService = TestBed.get(RentalService);
      expectedRentals = [
        {id: 1,
          customerId: 1,
          carId: 7,
          dateFrom: new Date('05/10/2018'),
          dateTo: new Date('05/25/2018'),
          status: 'accepted',
          wifi: 'YES',
          gps: 'YES',
          additionalInsurance: 'YES'
        },

        {id: 2,
          customerId: 3,
          carId: 2,
          dateFrom: new Date('07/15/2018'),
          dateTo: new Date('08/25/2018'),
          status: 'accepted',
          wifi: 'NO',
          gps: 'YES',
          additionalInsurance: 'NO'
        },
      ] as Rental[];
    });

    it('should return expected rentals (called once)', () => {
      rentalService.getRentals().subscribe(
        r => expect(r).toEqual(expectedRentals, 'should return expected rentals'),
        fail
      );

      // RentalService should have made one request to GET cars from expected URL
      const req = httpTestingController.expectOne(rentalService.rentalsUrl);
      expect(req.request.method).toEqual('GET');

    });

    it('should return expected rentals (called multiple times)', () => {
      rentalService.getRentals().subscribe();
      rentalService.getRentals().subscribe();
      rentalService.getRentals().subscribe(
        r => expect(r).toEqual(expectedRentals, 'should return expected rentals'),
        fail
      );

      const requests = httpTestingController.match(rentalService.rentalsUrl);
      expect(requests.length).toEqual(3, 'calls to getRentals()');

    });
  });

  describe('#getRental', () => {

    it('should get rental', () => {

      const newRental: Rental = {
        id: 1,
        customerId: 1,
        carId: 7,
        dateFrom: new Date('05/10/2018'),
        dateTo: new Date('05/25/2018'),
        status: 'accepted',
        wifi: 'YES',
        gps: 'YES',
        additionalInsurance: 'YES'
      };

      rentalService.addRental(newRental);

      rentalService.getRental(1).subscribe(
        data => expect(data).toEqual(newRental, 'should return the rental'),
        fail
      );

      const req = httpTestingController.expectOne(rentalService.rentalsUrl + '/1');
      expect(req.request.method).toEqual('GET');
    });
  });

  describe('#updateRental', () => {

    it('should update a rental and return it', () => {

      const updateRental: Rental = {
        id: 1,
        customerId: 1,
        carId: 7,
        dateFrom: new Date('05/10/2018'),
        dateTo: new Date('05/25/2018'),
        status: 'accepted',
        wifi: 'YES',
        gps: 'YES',
        additionalInsurance: 'YES'
      };

      rentalService.updateRental(updateRental).subscribe(
        data => expect(data).toEqual(updateRental, 'should return the rental'),
        fail
      );

      // RentalService should have made one request to PUT hero
      const req = httpTestingController.expectOne(rentalService.rentalsUrl);
      expect(req.request.method).toEqual('PUT');
      expect(req.request.body).toEqual(updateRental);

    });

  });

  describe('#addRental', () => {

    it('should add a rental and return it', () => {

      const addRental: Rental = {
        id: 1,
        customerId: 1,
        carId: 7,
        dateFrom: new Date('05/10/2018'),
        dateTo: new Date('05/25/2018'),
        status: 'accepted',
        wifi: 'YES',
        gps: 'YES',
        additionalInsurance: 'YES'
      };

      rentalService.addRental(addRental).subscribe(
        data => expect(data).toEqual(addRental, 'should return the rental'),
        fail
      );

      // RentalService should have made one request to POST rental
      const req = httpTestingController.expectOne(rentalService.rentalsUrl);
      expect(req.request.method).toEqual('POST');
      expect(req.request.body).toEqual(addRental);

    });
  });

  describe('#deleteRental', () => {

    it('should delete a car and return it', () => {

      const newRental: Rental = {
        id: 1,
        customerId: 1,
        carId: 7,
        dateFrom: new Date('05/10/2018'),
        dateTo: new Date('05/25/2018'),
        status: 'accepted',
        wifi: 'YES',
        gps: 'YES',
        additionalInsurance: 'YES'
      };

      rentalService.addRental(newRental);

      rentalService.deleteRental(newRental).subscribe(
        data => expect(data).toEqual(newRental, 'should return the rental'),
        fail
      );

      // CarService should have made one request to POST car
      const req = httpTestingController.expectOne(rentalService.rentalsUrl + '/1');
      expect(req.request.method).toEqual('DELETE');
    });
  });
});
