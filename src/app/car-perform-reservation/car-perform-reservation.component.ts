import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Car} from '../car';
import {Location} from '@angular/common';
import {CarService} from '../car.service';
import {CarReservationService} from '../car-reservation.service';
import { RentalService} from '../rental.service';
import {Rental} from '../rentals';
import { Router } from '@angular/router';
import {CustomerService} from '../customer.service';
import {Customer} from '../customer';

@Component({
  selector: 'app-car-perform-reservation',
  templateUrl: './car-perform-reservation.component.html',
  styleUrls: ['./car-perform-reservation.component.css']
})
export class CarPerformReservationComponent implements OnInit {

  car: Car;
  customer: Customer;

  dateTo: Date;
  dateFrom: Date;
  rentals: Rental[];
  customers: Customer[];

  currentCustomerId = this.customerService.getCustomerId();

  constructor(
    private route: ActivatedRoute,
    private carService: CarService,
    private carReservationService: CarReservationService,
    private location: Location,
    private rentalService: RentalService,
    private router: Router,
    private customerService: CustomerService,
  ) { }

  ngOnInit(): void {
    this.getCar();
    this.rentalService.getRentals().subscribe(rentals => this.rentals = rentals);
    this.customerService.getCustomers().subscribe(customers => this.customers = customers);

    this.getCurrentCustomer();
  }

  getCar(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.carService.getCar(id).subscribe(
      car => {
        this.car = car;
        this.car.servicePeriod = new Date(this.car.servicePeriod);
        this.car.dateOfRetirement = new Date(this.car.dateOfRetirement);
        this.car.dateOfProduction = new Date(this.car.dateOfProduction);
        this.car.id = Number(this.car.id);
        this.car.mileage = Number(this.car.mileage);
      }
    );
  }

  getCurrentCustomer(): void {
    const id = this.currentCustomerId;
    this.customerService.getCustomer(id).subscribe(
      customer => {
        this.customer = customer,
          this.customer.creditCardNumber = Number(this.customer.creditCardNumber),
          this.customer.id = Number(this.customer.id);
      }
    );
  }

  goBack(): void {
    this.location.back();
  }

  makeReservation(): void {
    if (this.carReservationService.validateIfDateFromIsBeforeDateTo(this.dateFrom, this.dateTo) &&
      this.carReservationService.validateIfCarIsNotReservedOnThisDate(this.dateFrom, this.dateTo, this.car.id, this.rentals) &&
      this.carReservationService.validateIfServiceDateIsNotBetweenDateFromAndDateTo(this.dateFrom, this.dateTo, this.car.servicePeriod)) {
      // alert('walidacja daty dziala');

      this.addNewRental(this.customerService.getCustomerId(), this.car.id, this.dateFrom, this.dateTo, '');

      this.customer.currentRentals.push(this.car.id);
      this.customerService.updateCustomer(this.customer).subscribe();
      this.router.navigate(['reservations']);
    }
  }

  addNewRental(customerId: number, carId: number, dateFrom: Date,
      dateTo: Date, status: string): void {

    if (!customerId && !carId && !dateFrom && !dateTo && !status) {
      return;
    }
    this.rentalService.addRental({ customerId, carId, dateFrom, dateTo, status } as Rental)
      .subscribe(rental => {
        this.rentals.push(rental);
      }, );
    this.router.navigate(['reservations']);
  }

  onValueChangeFrom(value: Date): void {
    this.dateFrom = value;
  }

  onValueChangeTo(value: Date): void {
    this.dateTo = value;
  }

}
