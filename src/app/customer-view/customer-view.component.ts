import {Component, OnInit} from '@angular/core';
import { CustomerService } from '../customer.service';
import { Customer } from '../customer';
import { Router} from '@angular/router';
import {Rental} from '../rentals';
import { RentalService } from '../rental.service';

@Component({
  selector: 'app-customer-view',
  templateUrl: './customer-view.component.html',
  styleUrls: ['./customer-view.component.css']
})
export class CustomerViewComponent implements OnInit {

  customers: Customer[];

  customer: Customer;

  rentals: Rental[];

  currentCustomerId = this.customerService.getCustomerId();
  currentCustomerIdToCheck = this.customerService.getCustomerIdInString();

  key = 'name';
  reverse = false;
  sort(key) {
    this.key = key;
    this.reverse = !this.reverse;
  }

  constructor(
    private customerService: CustomerService,
    private router: Router,
    private rentalService: RentalService
  ) { }

  ngOnInit() {
    this.getCustomers();
    this.getCurrentCustomer();
    this.getRentals();
  }

  getCustomers(): void {
    this.customerService.getCustomers().subscribe(customers => this.customers = customers);
  }

  getCurrentCustomer(): void {
    const id = this.currentCustomerId;
    this.customerService.getCustomer(id).subscribe(
      customer => {
        this.customer = customer,
          this.customer.creditCardNumber = Number(this.customer.creditCardNumber),
          this.customer.id = Number(this.customer.id);
      }
    );
  }

  getRentals(): void {
    this.rentalService.getRentals().subscribe(rentals => this.rentals = rentals);
  }

  logout(): void {
    this.customerService.setCustomerLoggedOut();
    this.customerService.setCustomerId(-1);
    this.router.navigate(['/']);
  }
}
