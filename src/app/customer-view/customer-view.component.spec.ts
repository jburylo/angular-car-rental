import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { CustomerViewComponent} from './customer-view.component';
import { Router} from '@angular/router';
import {CustomerService} from '../customer.service';
import {defer} from 'rxjs/observable/defer';
import {RentalService} from '../rental.service';
import { Ng2SearchPipeModule} from 'ng2-search-filter';
import {Ng2OrderModule } from 'ng2-order-pipe';

describe('CustomerViewComponent', () => {
  let component: CustomerViewComponent;
  let fixture: ComponentFixture<CustomerViewComponent>;

  const rentalSpy = jasmine.createSpyObj('RentalService', ['getRentals']);
  const customerSpy = jasmine.createSpyObj('CustomerService', ['getCustomers', 'setCustomerLoggedOut', 'setCustomerId'
    , 'getCustomer', 'getCustomerId', 'getCustomerIdInString']);
  const routerSpy = jasmine.createSpyObj('Router', ['navigate']);

  const customers = [
    {id: 1,
      name: 'Jakub',
      surname: 'Buryło',
      email: 'jakub.burylo@rentalcars.com',
      creditCardNumber: 12418725032,
      login: 'user1',
      password: 'user1',
      currentRentals: [7]
    },

    {id: 2,
      name: 'Marian',
      surname: 'Patyk',
      email: 'marian.patyk@rentalcars.com',
      creditCardNumber: 12124651116,
      login: 'user2',
      password: 'user2',
      currentRentals: []
    },

    {id: 3,
      name: 'Janusz',
      surname: 'Nosacz',
      email: 'janusz.nosacz@rentalcars.com',
      creditCardNumber: 12161618932,
      login: 'user3',
      password: 'user3',
      currentRentals: [2]
    },

    {id: 4,
      name: 'Ferdek',
      surname: 'Kiepski',
      email: 'ferdek.kiepski@rentalcars.com',
      creditCardNumber: 36364425032,
      login: 'user4',
      password: 'user4',
      currentRentals: [5]
    }
  ];

  const customer = [
    {id: 1,
      name: 'Jakub',
      surname: 'adad',
      email: 'jakub.burylo@rentalcars.com',
      creditCardNumber: 12418725032,
      login: 'user1',
      password: 'user1',
      currentRentals: [7]
    }
    ];

  const rentals = [
    {id: 1,
      customerId: 1,
      carId: 7,
      dateFrom: new Date('05/10/2018'),
      dateTo: new Date('05/25/2018'),
      status: 'accepted',
      wifi: 'YES',
      gps: 'YES',
      additionalInsurance: 'YES'
    },

    {id: 2,
      customerId: 3,
      carId: 2,
      dateFrom: new Date('07/15/2018'),
      dateTo: new Date('08/25/2018'),
      status: 'accepted',
      wifi: 'NO',
      gps: 'YES',
      additionalInsurance: 'NO'
    },

    {id: 3,
      customerId: 4,
      carId: 5,
      dateFrom: new Date('01/19/2018'),
      dateTo: new Date('02/15/2018'),
      status: 'declined',
      wifi: 'NO',
      gps: 'NO',
      additionalInsurance: 'NO'
    }
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, FormsModule, Ng2SearchPipeModule, Ng2OrderModule],
      declarations: [ CustomerViewComponent ],
      providers: [
        {provide: RentalService, useValue: rentalSpy},
        {provide: CustomerService, useValue: customerSpy},
        {provide: Router, useValue: routerSpy}
      ]
    })
      .compileComponents();

    customerSpy.getCustomers.and.returnValue(asyncData(customers));
    rentalSpy.getRentals.and.returnValue(asyncData(rentals));
    customerSpy.getCustomer.and.returnValue(asyncData(customer));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('#getCustomers', () => {

    it('should get all customers', fakeAsync(() => { // zamarkowanie operacji asynchronicznej
      // given
      // when
      component.ngOnInit();
      tick(); // wykonanie się operacji subscribe
      // then
      expect(component.customers).toEqual(customers);
    }));
  });

  describe('#getRentals', () => {

    it('should get all rentals', fakeAsync(() => { // zamarkowanie operacji asynchronicznej
      // given
      // when
      component.ngOnInit();
      tick(); // wykonanie się operacji subscribe
      // then
      expect(component.rentals).toEqual(rentals);
    }));
  });

  function asyncData<T>(data: T) {
    return defer(() => Promise.resolve(data));
  }
});
