import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; // <-- NgModel lives here
import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './in-memory-data.service';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { Ng2OrderModule } from 'ng2-order-pipe';

import { CarService } from './car.service';

import { AppComponent } from './app.component';
import { CarsComponent } from './cars/cars.component';
import { CarDetailComponent } from './car-detail/car-detail.component';
import { AppRoutingModule } from './/app-routing.module';
import { LoginFormComponent} from './login-form/login-form.component';
import { AdminService } from './admin.service';
import { AuthguardGuard } from './authguard.guard';
import { CustomersComponent } from './customers/customers.component';
import { CustomerService } from './customer.service';
import { CustomerDetailComponent } from './customer-detail/customer-detail.component';
import { RentalsComponent } from './rentals/rentals.component';
import { CarsReservationComponent } from './cars-reservation/cars-reservation.component';
import { CarPerformReservationComponent } from './car-perform-reservation/car-perform-reservation.component';
import { AlertModule } from 'ngx-bootstrap';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ModalModule } from 'ngx-bootstrap/modal';
import { RentalService } from './rental.service';
import { RentDetailComponent } from './rent-detail/rent-detail.component';
import {AuthguardcustomerGuard} from './authguardcustomer.guard';
import {CarReservationService} from './car-reservation.service';
import { CustomerViewComponent } from './customer-view/customer-view.component';

@NgModule({
  declarations: [
    AppComponent,
    CarsComponent,
    CarDetailComponent,
    LoginFormComponent,
    CustomersComponent,
    CustomerDetailComponent,
    RentalsComponent,
    CarsReservationComponent,
    CarPerformReservationComponent,
    RentDetailComponent,
    CustomerViewComponent
  ],
  imports: [
    ModalModule.forRoot(),
    BsDatepickerModule.forRoot(),
    AlertModule.forRoot(),
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(InMemoryDataService, { dataEncapsulation: false }),
    Ng2SearchPipeModule,
    Ng2OrderModule
  ],
  providers: [
    AdminService,
    CarService,
    CustomerService,
    RentalService,
    AuthguardcustomerGuard,
    AuthguardGuard,
    CarReservationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
