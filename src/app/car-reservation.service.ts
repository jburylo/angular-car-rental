import {Injectable } from '@angular/core';

@Injectable()
export class CarReservationService {

  constructor() {}

  validateIfDateFromIsBeforeDateTo(dateFrom, dateTo): boolean {
    if (dateFrom > dateTo) {
      alert('Starting date cannot be before end date!');
      return false;
    } else {
      return true;
    }
  }

  validateIfServiceDateIsNotBetweenDateFromAndDateTo(dateFrom, dateTo, serviceDate): boolean {
    const serviceDateToCheck = new Date(serviceDate);
    if (dateFrom < serviceDateToCheck && dateTo > serviceDateToCheck) {
      alert('You cannot rent this car! Between dates you have picked, car is servicing!');
      return false;
    } else {
      return true;
    }
  }

  validateIfServiceDateIsNotBetweenDateFromAndDateToSecondVersion(dateFrom, dateTo, carId, cars): boolean {

    const carIdToCheck = Number(carId);
    let serviceDateToCheck: Date;
    for (let i = 0; i < cars.length; i ++) {
      const currentCarId = Number(cars[i].id);
      if (currentCarId === carIdToCheck) {
        serviceDateToCheck = new Date(cars[i].servicePeriod);
      }
    }
    if (dateFrom < serviceDateToCheck && dateTo > serviceDateToCheck) {
      alert('You cannot rent this car! Between dates you have picked, car is servicing!');
      return false;
    } else {
      return true;
    }
  }

  validateIfCarIsNotReservedOnThisDate(dateFromToCheck, dateToToCheck, carId, rentals): boolean {
    const carIdToCheck = Number(carId);
    for (let i = 0; i < rentals.length; i++) {
      const currentCarId = Number(rentals[i].carId);
      if (currentCarId === carIdToCheck) {
        const reservedDateFrom = new Date(rentals[i].dateFrom);
        const reservedDateTo = new Date(rentals[i].dateTo);
        if (!(dateToToCheck < reservedDateFrom || dateFromToCheck > reservedDateTo)) {
          alert('This car is already reserved in this date! Please choose another date!');
          return false;
        }
      }
    }
    return true;
  }

  // validateIfUserExist(customerId, customers): boolean {
  //   for (let i = 0; i < customers.length; i++) {
  //     if (customerId === customers[i].id) {
  //       return true;
  //     }
  //   }
  //   alert('This user does not exist!');
  //   return false;
  // }

  // validateIfCarExist(carId, cars): boolean {
  //   for (let i = 0; i < cars.length; i++) {
  //     if (carId === cars[i].id) {
  //       return true;
  //     }
  //   }
  //   alert('This car does not exist!');
  //   return false;
  // }

}
