import {Component, OnInit, TemplateRef} from '@angular/core';
import { Customer } from '../customer';
import {CustomerService} from '../customer.service';
import { AdminService } from '../admin.service';
import { Router } from '@angular/router';
import {CarService} from '../car.service';
import {Car} from '../car';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {

  customers: Customer[];
  cars: Car[];
  customerCars: Car[];
  modalRef: BsModalRef;

  key = 'name';
  reverse = false;

  constructor(
    private customerService: CustomerService,
    private carService: CarService,
    private adminService: AdminService,
    private modalService: BsModalService,
    private router: Router
  ) { }

  sort(key) {
    this.key = key;
    this.reverse = !this.reverse;
  }

  getCustomers(): void {
    this.customerService.getCustomers().subscribe(customers => this.customers = customers);
  }

  getCars(): void {
    this.carService.getCars().subscribe(cars => this.cars = cars);
  }

  getCustomerCars(currentRentals, template): void {
    this.customerCars = new Array<Car>();
    for (let i = 0; i < this.cars.length; i++) {
      for (let j = 0; j < currentRentals.length; j++) {
        if (this.cars[i].id === currentRentals[j]) {
          this.customerCars.push(this.cars[i]);
        }
      }
    }
    this.openModal(template);
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  logout(): void {
    this.adminService.setAdminLoggedOut();
    this.router.navigate(['/']);
  }

  delete(customer: Customer): void {
    this.customers = this.customers.filter(c => c !== customer);
    this.customerService.deleteCustomer(customer).subscribe();
  }

  add(name: string, surname: string, email: string,
      creditCardNumber: number, login: string, password: string, currentRentals: number[]): void {

    currentRentals = new Array<number>();
    if (!name || !surname || !email || !creditCardNumber || !login || !password) { return; }
    this.customerService.addCustomer({ name, surname, email, creditCardNumber, login, password, currentRentals } as Customer)
      .subscribe(customer => {
        this.customers.push(customer);
      });
  }

  ngOnInit() {
    this.getCustomers();
    this.getCars();
    this.customerCars = new Array<Car>();
  }
}
