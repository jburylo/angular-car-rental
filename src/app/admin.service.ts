import { Injectable } from '@angular/core';

@Injectable()
export class AdminService {

  isAdminLoggedIn;

  constructor() {
    this.isAdminLoggedIn = false;
  }

  setAdminLoggedIn() {
    this.isAdminLoggedIn = true;
  }

  setAdminLoggedOut() {
    this.isAdminLoggedIn = false;
  }

  getAdminLoggedIn() {
    return this.isAdminLoggedIn;
  }

}
