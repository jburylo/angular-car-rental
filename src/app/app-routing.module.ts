import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CarsComponent } from './cars/cars.component';
import { CarDetailComponent } from './car-detail/car-detail.component';
import {AuthguardGuard} from './authguard.guard';
import {LoginFormComponent} from './login-form/login-form.component';
import {CustomersComponent} from './customers/customers.component';
import {CustomerDetailComponent} from './customer-detail/customer-detail.component';
import {RentalsComponent} from './rentals/rentals.component';
import {CarsReservationComponent} from './cars-reservation/cars-reservation.component';
import {CarPerformReservationComponent} from './car-perform-reservation/car-perform-reservation.component';
import {RentDetailComponent} from './rent-detail/rent-detail.component';
import {AuthguardcustomerGuard} from './authguardcustomer.guard';
import {CustomerViewComponent} from './customer-view/customer-view.component';

const routes: Routes = [
  { path: '', component: LoginFormComponent },
  { path: 'cars', canActivate: [AuthguardGuard], component: CarsComponent },
  { path: 'customers', canActivate: [AuthguardGuard], component: CustomersComponent },
  { path: 'rentals', canActivate: [AuthguardGuard], component: RentalsComponent },
  { path: 'detail/:id', canActivate: [AuthguardGuard], component: CarDetailComponent },
  { path: 'detailCustomer/:id', canActivate: [AuthguardGuard], component: CustomerDetailComponent },
  { path: 'detailRent/:id', canActivate: [AuthguardGuard], component: RentDetailComponent },
  { path: 'reservations', canActivate: [AuthguardcustomerGuard], component: CarsReservationComponent },
  { path: 'performReservation/:id', canActivate: [AuthguardcustomerGuard], component: CarPerformReservationComponent },
  { path: 'customerView', canActivate: [AuthguardcustomerGuard], component: CustomerViewComponent}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes)],
  exports: [ RouterModule ]
})

export class AppRoutingModule { }
