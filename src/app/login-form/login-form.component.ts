import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {AdminService} from '../admin.service';
import {CustomerService} from '../customer.service';
import { Customer } from '../customer';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  welcome = 'Welcome. Please login';

  customers: Customer[];

  constructor(private router: Router, private admin: AdminService, private customer: CustomerService) { }

  ngOnInit() {
    this.customer.getCustomers().subscribe(customers => this.customers = customers);
  }

  loginUser(username: string, password: string) {

    if (username === 'admin' && password === 'admin') {
      this.admin.setAdminLoggedIn();
      this.router.navigate(['cars']);
      return true;
    } else if (!this.admin.isAdminLoggedIn) {
      for (let i = 0; i < this.customers.length; i++) {
        if (username === this.customers[i].login && password === this.customers[i].password) {
          this.customer.setCustomerLoggedIn();
          this.customer.setCustomerId(this.customers[i].id);
          this.router.navigate(['customerView']);
          return true;
        }
      }
    }
    if (!this.admin.isAdminLoggedIn && !this.customer.isCustomerLoggedIn) {
        alert('User does not exist!');
        return false;
    }
  }

  add(name: string, surname: string, email: string,
      creditCardNumber: number, login: string, password: string, currentRentals: number[]): void {
    currentRentals = [];
    if (!name || !surname || !email || !creditCardNumber || !login || !password) { return; }
    this.customer.addCustomer({ name, surname, email, creditCardNumber, login, password, currentRentals} as Customer)
      .subscribe(customer => {
        this.customers.push(customer);
      });
  }

}
