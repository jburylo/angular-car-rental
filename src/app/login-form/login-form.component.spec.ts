import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {BsModalService, ModalModule} from 'ngx-bootstrap/modal';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { LoginFormComponent } from './login-form.component';
import {Router} from '@angular/router';
import {AdminService} from '../admin.service';
import {CustomerService} from '../customer.service';
import {defer} from 'rxjs/observable/defer';

describe('LoginFormComponent', () => {
  let component: LoginFormComponent;
  let fixture: ComponentFixture<LoginFormComponent>;
  const adminSpy = jasmine.createSpyObj('AdminService', ['setAdminLoggedIn']);
  const customerSpy = jasmine.createSpyObj('CustomerService', ['getCustomers', 'setCustomerLoggedIn'
    , 'setCustomerId', 'addCustomer', 'subscribe']);
  const routerSpy = jasmine.createSpyObj('Router', ['navigate']);

  const customers = [
    {id: 1,
      name: 'Jakub',
      surname: 'Buryło',
      email: 'jakub.burylo@rentalcars.com',
      creditCardNumber: 12418725032,
      login: 'user1',
      password: 'user1',
      currentRentals: [7]
    },

    {id: 2,
      name: 'Marian',
      surname: 'Patyk',
      email: 'marian.patyk@rentalcars.com',
      creditCardNumber: 12124651116,
      login: 'user2',
      password: 'user2',
      currentRentals: []
    },

    {id: 3,
      name: 'Janusz',
      surname: 'Nosacz',
      email: 'janusz.nosacz@rentalcars.com',
      creditCardNumber: 12161618932,
      login: 'user3',
      password: 'user3',
      currentRentals: [2]
    },

    {id: 4,
      name: 'Ferdek',
      surname: 'Kiepski',
      email: 'ferdek.kiepski@rentalcars.com',
      creditCardNumber: 36364425032,
      login: 'user4',
      password: 'user4',
      currentRentals: [5]
    }
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, FormsModule, ModalModule.forRoot(), ],
      declarations: [ LoginFormComponent ],
      providers: [
        {provide: AdminService, useValue: adminSpy},
        {provide: CustomerService, useValue: customerSpy},
        {provide: Router, useValue: routerSpy}
      ]
    })
    .compileComponents();
    customerSpy.getCustomers.and.returnValue(asyncData(customers));
  }));

  beforeEach(() => {
    spyOn(window, 'alert');
    fixture = TestBed.createComponent(LoginFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('#loginUser', () => {

    it('should login as admin', () => {
      component.customers = customers;

      expect(component.loginUser('admin', 'admin')).toEqual(true);
      expect(routerSpy.navigate).toHaveBeenCalledWith(['cars']);
    });

    it('should login as customer', () => {
      component.customers = customers;

      expect(component.loginUser('user2', 'user2')).toEqual(true);
      expect(routerSpy.navigate).toHaveBeenCalledWith(['customerView']);
    });

    it('should login failed', () => {
      component.customers = customers;

      expect(component.loginUser('kuba', 'kuba')).toEqual(false);
      expect(window.alert).toHaveBeenCalledWith('User does not exist!');
    });

  });

  function asyncData<T>(data: T) {
    return defer(() => Promise.resolve(data));
  }
});

