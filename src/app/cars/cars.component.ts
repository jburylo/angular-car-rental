import { Component, OnInit } from '@angular/core';
import {Car} from '../car';
import { CarService } from '../car.service';
import { AdminService } from '../admin.service';
import { Router} from '@angular/router';

@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.css']
})
export class CarsComponent implements OnInit {

  cars: Car[];

  key = 'name';
  reverse = false;
  sort(key) {
    this.key = key;
    this.reverse = !this.reverse;
  }

  getCars(): void {
    this.carService.getCars().subscribe(cars => this.cars = cars);
  }

  delete(car: Car): void {
    this.cars = this.cars.filter(c => c !== car);
    this.carService.deleteCar(car).subscribe();
  }

  constructor(
    private carService: CarService,
    private adminService: AdminService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.getCars();
  }

  logout(): void {
    this.adminService.setAdminLoggedOut();
    this.router.navigate(['/']);
  }

  add(brand: string, model: string, description: string,
      colour: string, dateOfProduction: Date, dateOfRetirement: Date,
      mileage: number, servicePeriod: Date, status: string): void {

    if (!brand || !model || !description || !colour || !dateOfProduction || !dateOfRetirement || !mileage || !servicePeriod || !status) {
      return;
    }
    this.carService.addCar({ brand, model, description, colour, dateOfProduction, dateOfRetirement, mileage, servicePeriod, status } as Car)
      .subscribe(car => {
        this.cars.push(car);
      });
  }

}
